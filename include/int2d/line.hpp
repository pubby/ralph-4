#ifndef INT2D_LINE_HPP
#define INT2D_LINE_HPP

// Generic Bressenham line algorithm code.

#include <cassert>
#include <cstdlib>
#include <functional>
#include <iterator>

#include "geometry.hpp"
#include "units.hpp"

namespace int2d {

    // A wrapper around the state of Bressenham's line algorithm.
    // NOTE: A direction vector of {0,0} is considered invalid.
    struct line_state
    {
        coord position;
        coord direction;
        int error;

        // Use these to construct 'line_state'.
        // Aggregate initialization isn't recommended because 'error' needs
        // to be set.
        static line_state pos_dir(coord pos, coord dir);
        static line_state from_to(coord from, coord to);

        // The starting 'error' value for a given direction.
        // Starts the line right in the middle.
        static int dir_err(coord direction);

        static line_state next(line_state line);
        static line_state next(line_state line, int n);
        static line_state prev(line_state line);
        static line_state prev(line_state line, int n);

        static line_state hflipped(line_state line);
        static line_state vflipped(line_state line);

        void advance() { *this = next(*this); }
        void advance(int n) { *this = next(*this, n); }
        void radvance() { *this = prev(*this); }
        void radvance(int n) { *this = prev(*this, n); }
        void hflip() { *this = hflipped(*this); }
        void vflip() { *this = vflipped(*this); }

        explicit operator bool() const { return direction != coord{0,0}; }
    };

    constexpr bool operator==(line_state lhs, line_state rhs)
    {
        return (lhs.position == rhs.position
                && lhs.direction == rhs.direction
                && lhs.error == rhs.error);
    }

    constexpr bool operator!=(line_state lhs, line_state rhs)
    {
        return !(lhs == rhs);
    }

    class line_iterator
    : public std::iterator<std::random_access_iterator_tag, coord const>
    {
        friend class line_range;
    public:
        line_iterator() : m_state{} {}
        explicit line_iterator(line_state state) : m_state(state) {}

        coord operator*() const { return m_state.position; }
        coord const* operator->() const { return &m_state.position; }

        line_iterator& operator+=(int n) { m_state.advance(n); return *this; }
        line_iterator& operator++() { m_state.advance(); return *this; }
        line_iterator operator++(int)
        {
            line_iterator ret(*this);
            ++(*this);
            return ret;
        }

        line_iterator& operator-=(int n) { m_state.radvance(n); return *this; }
        line_iterator& operator--() { m_state.radvance(); return *this; }
        line_iterator operator--(int)
        {
            line_iterator ret(*this);
            --(*this);
            return ret;
        }

        line_iterator operator+(int rhs) const
        {
            line_iterator lhs = *this;
            lhs += rhs;
            return lhs;
        }

        line_iterator operator-(int rhs) const
        {
            line_iterator lhs = *this;
            lhs -= rhs;
            return lhs;
        }

        coord operator[](std::size_t i) { return *(*this + i); }

        line_state state() const { return m_state; }
    private:
        line_state m_state;
    };

    std::size_t operator-(line_iterator lhs, line_iterator rhs);

    inline bool operator==(line_iterator lhs, line_iterator rhs)
    {
        assert(*lhs != *rhs || lhs.state() == rhs.state());
        return *lhs == *rhs;
    }

    inline bool operator!=(line_iterator lhs, line_iterator rhs)
    {
        return !(lhs == rhs);
    }

    bool operator<(line_iterator lhs, line_iterator rhs);
    bool operator<=(line_iterator lhs, line_iterator rhs);
    bool operator>(line_iterator lhs, line_iterator rhs);
    bool operator>=(line_iterator lhs, line_iterator rhs);

    class line_range
    {
    public:
        using const_iterator = line_iterator;

        line_range() = default;

        explicit line_range(coord crd)
        : m_begin({ crd, { 1, 0 }, 0 })
        , m_end({ crd + coord{ 1, 0 }, { 1, 0 }, 0 })
        {}

        line_range(coord from, coord to)
        : line_range(line_state::from_to(from, to), cdistance(from, to) + 1)
        {}

        // NOTE: A direction vector of {0,0} is considered invalid.
        line_range(coord pos, coord dir, int steps)
        : line_range(line_state::pos_dir(pos, dir), steps)
        {}

        line_range(line_state begin, int steps)
        : m_begin(begin)
        , m_end(line_state::next(begin, steps))
        {}

        line_iterator begin() const { return m_begin; }
        line_iterator end() const { return m_end; }

        line_iterator cbegin() const { return m_begin; }
        line_iterator cend() const { return m_end; }

        std::size_t size() const { return cend() - cbegin(); }

        coord first() const { return *m_begin; }
        coord last() const { return *(m_end - 1); }

        void lengthen() { ++m_end; }
        void shorten() { --m_end; }
    private:
        line_iterator m_begin;
        line_iterator m_end;
    };

    namespace impl
    {
        constexpr bool is_steep(coord dir)
        {
            return sqr(dir.y) > sqr(dir.x);
        }

        inline coord coord_abs(coord direction)
        {
            return { std::abs(direction.x), std::abs(direction.y) };
        }

        // The version of Bressenham being used requires x and y to swap when
        // the line is steeper than 45 degrees.
        // This function simplifies the swapping.
        template<typename Func>
        auto steep_swap(line_state line, Func func)
        {
            if(is_steep(line.direction))
                return func(line, component_index<1>{}, component_index<0>{});
            else
                return func(line, component_index<0>{}, component_index<1>{});
        }

        template<typename T>
        constexpr int _signum(T x, std::false_type)
        {
            return T(0) < x;
        }

        template<typename T>
        constexpr int _signum(T x, std::true_type)
        {
            return (T(0) < x) - (x < T(0));
        }

        // Returns either -1, 0, or 1, depending on sign of val.
        template<typename T>
        constexpr int signum(T x)
        {
            static_assert(std::is_arithmetic<T>::value,
                          "only implemented for arithmetic");
            return _signum(x, std::is_signed<T>());
        }
    } // namespace impl


    // Traces a line.
    // This may be slightly faster than line_state and line_range.
    // TODO: profile?
    void trace_line(coord from, coord to, std::function<void(coord)> it_func)
    {
        // Using a slightly different version of the algorithm than line_state.
        // This version doesn't need to swap x or y.
        coord const direction = to - from;
        coord const d = impl::coord_abs(direction);
        coord const s = mapc(direction, &impl::signum<int>);
        for(int err = d.x - d.y; it_func(from), from != to;)
        {
            int const err2 = 2 * err;
            if(err2 > -d.y)
            {
                err -= d.y;
                from.x += s.x;
            }
            if(err2 < d.x)
            {
                err += d.x;
                from.y += s.y;
            }
        }
    }

    line_state line_state::pos_dir(coord pos, coord dir)
    {
        return { pos, dir, dir_err(dir) };
    }

    line_state line_state::from_to(coord from, coord to)
    {
        if(from == to)
            return pos_dir(from, {1,0});
        else
            return pos_dir(from, to - from);
    }

    int line_state::dir_err(coord direction)
    {
        using namespace impl;
        return std::abs(is_steep(direction) ? direction.y : direction.x);
    }

    line_state line_state::next(line_state line)
    {
        using namespace impl;
        assert((line.direction != coord{0,0}));
        // This is 1 iteration of Bressenham's line algorithm.
        return impl::steep_swap(line,
            [](line_state line, auto cx, auto cy)
            {
                coord const d = impl::coord_abs(line.direction);
                line.position[cx] += impl::signum(line.direction[cx]);
                line.error -= d[cy] * 2;
                if(line.error < 0)
                {
                    line.position[cy] += impl::signum(line.direction[cy]);
                    line.error += d[cx] * 2;
                }
                return line;
            });
    }

    line_state line_state::prev(line_state line)
    {
        assert((line.direction != coord{0,0}));
        return impl::steep_swap(line,
            [](line_state line, auto cx, auto cy)
            {
                coord const d = impl::coord_abs(line.direction);
                line.position[cx] -= impl::signum(line.direction[cx]);
                line.error += d[cy] * 2;
                if(line.error > d[cx] * 2)
                {
                    line.position[cy] -= impl::signum(line.direction[cy]);
                    line.error -= d[cx] * 2;
                }
                return line;
            });
    }

    static line_state next_impl(line_state line, int n)
    {
        assert((line.direction != coord{0,0}));
        return impl::steep_swap(line,
            [n](line_state line, auto cx, auto cy)
            {
                coord const d2 = vec_mul(impl::coord_abs(line.direction), 2);
                line.position[cx] += n * impl::signum(line.direction[cx]);
                line.error -= d2[cy] * n;
                int const y_change = (d2[cx] - line.error - 1) / d2[cx];
                assert(y_change >= 0);
                line.position[cy] += y_change * impl::signum(line.direction[cy]);
                line.error += y_change * d2[cx];
                return line;
            });
    }

    static line_state prev_impl(line_state line, int n)
    {
        assert((line.direction != coord{0,0}));
        return impl::steep_swap(line,
            [n](line_state line, auto cx, auto cy)
            {
                coord const d2 = vec_mul(impl::coord_abs(line.direction), 2);
                line.position[cx] -= n * impl::signum(line.direction[cx]);
                line.error += d2[cy] * n;
                int const y_change = (line.error - 1) / d2[cx];
                assert(y_change >= 0);
                line.position[cy] -= y_change * impl::signum(line.direction[cy]);
                line.error -= y_change * d2[cx];
                return line;
            });
    }

    // This is the same as repeatedly calling next(line) n times,
    // except this function has O(1) complexity instead of O(1).
    line_state line_state::next(line_state line, int n)
    {
        if(n < 0)
            return prev_impl(line, -n);
        else
            return next_impl(line, n);
    }

    line_state line_state::prev(line_state line, int n)
    {
        if(n < 0)
            return next_impl(line, -n);
        else
            return prev_impl(line, n);
    }

    line_state line_state::hflipped(line_state line)
    {
        // TODO: Should error be updated?
        line.direction.x *= -1;
        return line;
    }

    line_state line_state::vflipped(line_state line)
    {
        // TODO: Should error be changed?
        line.direction.y *= -1;
        return line;
    }

    std::size_t operator-(line_iterator lhs, line_iterator rhs)
    {
        return cdistance(*lhs, *rhs);
    }

    static int iter_cmp(line_iterator lhs, line_iterator rhs)
    {
        coord p2 = *rhs;
        return impl::steep_swap(lhs.state(),
            [p2](line_state l1, auto cx, auto cy)
            {
                return (l1.position[cx] - p2[cx]) * l1.direction[cx];
            });
    }

    bool operator<(line_iterator lhs, line_iterator rhs)
    {
        return iter_cmp(lhs, rhs) < 0;
    }

    bool operator<=(line_iterator lhs, line_iterator rhs)
    {
        return iter_cmp(lhs, rhs) <= 0;
    }

    bool operator>(line_iterator lhs, line_iterator rhs)
    {
        return iter_cmp(lhs, rhs) > 0;
    }

    bool operator>=(line_iterator lhs, line_iterator rhs)
    {
        return iter_cmp(lhs, rhs) >= 0;
    }

} // namespace int2d

#endif
