#ifndef INT2D_GRID_HPP
#define INT2D_GRID_HPP

#include <algorithm>
#include <array>
#include <cassert>
#include <functional>
#include <iterator>
#include <utility>
#include <vector>

#include "geometry.hpp"

namespace int2d {

    namespace impl {
        template<typename T>
        struct to_void
        {
            using type = void;
        };

        template<typename T>
        using ToVoid = typename to_void<T>::type;
    }

    template<typename T, typename = void>
    struct is_grid : std::false_type {};

    template<typename T>
    struct is_grid<T, impl::ToVoid<typename T::is_grid> > : std::true_type {};

    template<typename T, int Width, int Height>
    class fixed_grid
    {
    private:
        using array_type = std::array<T, Width*Height>;
    public:
        using is_grid = void;
        using value_type = T;
        using iterator = typename array_type::iterator;
        using const_iterator = typename array_type::const_iterator;

        fixed_grid() = default;
        fixed_grid(T const& value)
        {
            for(T& t : m_arr)
                t = value;
        }

        fixed_grid(fixed_grid const&) = default;
        fixed_grid(fixed_grid&&) = default;

        fixed_grid& operator=(fixed_grid const&) = default;
        fixed_grid& operator=(fixed_grid&&) = default;

        const_iterator cbegin() const { return m_arr.begin(); }
        const_iterator cend() const { return m_arr.end(); }

        const_iterator begin() const { return cbegin(); }
        const_iterator end() const { return cend(); }

        iterator begin() { return m_arr.begin(); }
        iterator end() { return m_arr.end(); }

        constexpr dimen dimensions() const { return { Width, Height }; }

        T const& at(coord c) const { return m_arr.at(index(c)); }
        T& at(coord c) { return m_arr.at(index(c)); }

        T const& operator[](coord c) const { return m_arr[index(c)]; }
        T& operator[](coord c) { return m_arr[index(c)]; }

        T const* data() const { return m_arr.data(); }
        T* data() { return m_arr.data(); }

        std::size_t size() const { return m_arr.size(); }

    private:
        std::size_t index(coord c) const { return c.y * dimensions().w + c.x; }

        array_type m_arr;
    };

    // This is like a rectangular std::vector.
    // It has column-major ordering.
    template<typename T, class A = std::allocator<T> >
    class grid
    {
    private:
        using vector_type = std::vector<T, A>;
    public:
        using is_grid = void;
        using value_type = T;
        using allocator_type = A;

        using iterator = typename vector_type::iterator;
        using const_iterator = typename vector_type::const_iterator;

        grid()
        : grid(A())
        {}

        explicit grid(A const& alloc)
        : grid({0,0}, alloc)
        {}

        explicit grid(dimen dim, A const& alloc = A())
        : m_vec(area(dim), T(), alloc)
        , m_dim(dim)
        {}

        grid(dimen dim, T const& t, A const& alloc = A())
        : m_vec(area(dim), t, alloc)
        , m_dim(dim)
        {}

        grid(grid const&) = default;
        grid(grid&&) = default;

        grid& operator=(grid const&) = default;
        grid& operator=(grid&&) = default;

        allocator_type get_allocator() const { return m_vec.get_allocator(); }

        void swap(grid& other)
        {
            using std::swap;
            swap(m_vec, other.m_vec);
            swap(m_dim, other.m_dim);
        }

        friend void swap(grid& a, grid& b) noexcept
        {
            a.swap(b);
        }

        const_iterator cbegin() const { return m_vec.begin(); }
        const_iterator cend() const { return m_vec.end(); }

        const_iterator begin() const { return cbegin(); }
        const_iterator end() const { return cend(); }

        iterator begin() { return m_vec.begin(); }
        iterator end() { return m_vec.end(); }

        dimen dimensions() const { return m_dim; }

        T const& at(coord c) const { return m_vec.at(index(c)); }
        T& at(coord c) { return m_vec.at(index(c)); }

        T const& operator[](coord c) const { return m_vec[index(c)]; }
        T& operator[](coord c) { return m_vec[index(c)]; }

        T const* data() const { return m_vec.data(); }
        T* data() { return m_vec.data(); }

        std::size_t size() const { return m_vec.size(); }

        void resize(dimen new_dim)
        {
            grid new_grid(new_dim);
            dimen const copy_dim = crop(dimensions(), new_dim);
            for(auto crd : dimen_range(copy_dim))
                new_grid[crd] = operator[](crd);
            swap(new_grid);
        }

        void clear()
        {
            m_vec.clear();
            m_dim = {0,0};
        }

    private:
        std::size_t index(coord c) const { return c.y * m_dim.w + c.x; }

        vector_type m_vec;
        dimen m_dim;
    };

    // Blits one grid on top of another using merge_func to combine the values.
    // The signature of merge_func should be:
    // T(T const& dest_val, T const& src_val)
    template<typename Func, typename Grid>
    void fblit(Grid& dest,
               coord dest_crd,
               Grid const& src,
               rect src_rect,
               Func merge_func = Func())
    {
        static_assert(is_grid<Grid>::value, "must be a Grid");
        ASSERT(in_bounds(src_rect, src.to_rect()));
        ASSERT(in_bounds(src_rect, dest.to_rect()));
        for(int y = 0; y < src_rect.d.h; ++y)
        for(int x = 0; x < src_rect.d.w; ++x)
        {
            coord const dest_i = { dest_crd.x + x, dest_crd.y + y };
            coord const src_i = { src_rect.c.x + x, src_rect.c.y + y };
            dest[dest_i] = merge_func(static_cast<Grid const&>(dest)[dest_i],
                                      src[src_i]);
        }
    }

    template<typename Grid>
    void blit(Grid& dest,
              coord dest_crd,
              Grid const& src,
              rect src_rect)
    {
        static_assert(is_grid<Grid>::value, "must be a Grid");
        using value_type = typename Grid::value_type;
        fblit(
            dest,
            dest_crd,
            src,
            src_rect,
            [](value_type const&, value_type const& src_val)
            {
                return src_val;
            });
    }

    template<typename Func, typename Grid>
    void fblit(Grid& dest,
               coord dest_crd,
               Grid const& src,
               Func merge_func = Func())
    {
        static_assert(is_grid<Grid>::value, "must be a Grid");
        fblit(dest, dest_crd, src, src.to_rect(), merge_func);
    }

    template<typename Grid>
    void blit(Grid& dest,
              coord dest_crd,
              Grid const& src)
    {
        blit(dest, dest_crd, src, src.to_rect());
    }

    namespace impl
    {
        std::vector<std::string> lines_of(std::string str)
        {
            std::vector<std::string> ret;
            std::string temp;
            for(char ch : str)
            {
                if(ch == '\n')
                {
                    ret.push_back(temp);
                    temp.clear();
                }
                else
                    temp.push_back(ch);
            }
            ret.push_back(temp);
            return ret;
        }
    } // namespace impl

    grid<char> string_to_grid(std::string str)
    {
        auto lines = impl::lines_of(str);
        dimen dim = { 0, static_cast<int>(lines.size()) };
        for(auto const& line : lines)
            dim.w = std::max<int>(dim.w, line.size());

        grid<char> ret(dim, '\0');

        for(int y = 0; y < dim.h; ++y)
        for(int x = 0; x < static_cast<int>(lines[y].size()); ++x)
            ret[{x,y}] = lines[y][x];

        return ret;
    }

}

#endif
