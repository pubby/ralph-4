#include <stdio.h> /* for fprintf() */
#include <stdlib.h> /* for atexit() */
#include <utility>
#include <memory>
#include <iostream>
#include <chrono>

#include <SDL2/SDL.h>

#include <scope_guard.hpp>

#include <int2d/grid.hpp>
#include <int2d/pretty_print.hpp>

using namespace int2d;

constexpr int tile_size = 32;
constexpr int fps_cap = 30;

template<typename T, typename D>
std::unique_ptr<T, D> to_unique(T* ptr, D&& d) {
    return std::unique_ptr<T, D>(ptr, std::forward<D>(d));
}

enum terrain_type
{
    TER_NONE,
    TER_EXIT,
    TER_WALL,
    TER_UP,
    TER_DOWN,
    TER_LEFT,
    TER_RIGHT,
    TER_SPIKES,
};

struct ralph_t
{
    coord start_position;
    coord position;
    coord movement;
};

enum enemy_type
{
    ENEMY_H1,
};

struct enemy_t
{
    coord position;
    coord movement;
};

using map_type = fixed_grid<terrain_type, 20, 15>;

struct level_t
{
    map_type map;
    ralph_t ralph;
    std::vector<enemy_t> enemies;
};

/*
struct resources_t
{
    std::unique_ptr<SDL_Texture, void(SDL_Texture*)> tiles;
};
*/

coord texture_index(terrain_type wall)
{
    switch(wall)
    {
    default:
    case TER_NONE: return {0,0};
    case TER_EXIT: return {2,0};
    case TER_WALL: return {3,0};
    case TER_UP:   return {4,0};
    case TER_DOWN: return {5,0};
    case TER_LEFT: return {6,0};
    case TER_RIGHT: return {7,0};
    case TER_SPIKES: return {0,1};
    }
}

void draw_level(level_t const& level, SDL_Renderer* renderer)
{
}

level_t load_level(std::string level)
{
    level_t ret = {};
    auto chars = string_to_grid(level);
    chars.resize(ret.map.dimensions());
    for(coord i : dimen_range(ret.map.dimensions()))
    {
        switch(chars[i])
        {
        case '#': ret.map[i] = TER_WALL; break;
        case '%': ret.map[i] = TER_EXIT; break;
        case '^': ret.map[i] = TER_UP; break;
        case 'v': ret.map[i] = TER_DOWN; break;
        case '<': ret.map[i] = TER_LEFT; break;
        case '>': ret.map[i] = TER_RIGHT; break;
        case '*': ret.map[i] = TER_SPIKES; break;
        case '@': ret.ralph.start_position = vec_mul(i, tile_size); break;
        case '-': 
            ret.enemies.push_back({ vec_mul(i, tile_size), { 0, 0 } });
            break;
        default: break;
        }
    }
    ret.ralph.position = ret.ralph.start_position;
    return ret;
}

constexpr char const* level1 =
"####################\n"
"#   <<<   <<<     ##\n"
"#   #########   # ##\n"
"#   #  vvv >*   # ##\n"
"#   #  ***  *   # ##\n"
"#   #  ***  *   # ##\n"
"#      ***      # ##\n"
"#   ############# ##\n"
"#   ############# ##\n"
"#   ###     ##### ##\n"
"#              ## ##\n"
"#              ## ##\n"
"#        @     ## ##\n"
"#######     #####%##\n"
"####################";

std::vector<terrain_type> terrain_collisions(level_t const& level, rect r)
{
    std::vector<terrain_type> ret;
    for(int y = r.c.y / tile_size; y <= r.ry() / tile_size; ++y)
    for(int x = r.c.x / tile_size; x <= r.rx() / tile_size; ++x)
    {
        auto const t = level.map[{x, y}];
        if(t != TER_NONE
           && std::find(ret.begin(), ret.end(), t) == ret.end())
        {
            ret.push_back(t);
        }
    }
    return ret;
}

rect ralph_bbox(coord pos) 
{
    return rect_margin({ pos, { tile_size, tile_size }}, 4, 4);
}

coord try_move(level_t const& level, coord pos, coord movement)
{
    dimen dim = { 32, 32 };
    auto tc = terrain_collisions(level, { pos + movement, dim });
    if(std::find(tc.begin(), tc.end(), TER_WALL) == tc.end())
        return pos + movement;

    coord prev = pos;
    for(coord crd : line_range(pos, pos + movement))
    {
        tc = terrain_collisions(level, {{ crd.x, prev.y }, dim});
        if(std::find(tc.begin(), tc.end(), TER_WALL) == tc.end())
            prev.x = crd.x;

        tc = terrain_collisions(level, {{ prev.x, crd.y }, dim});
        if(std::find(tc.begin(), tc.end(), TER_WALL) == tc.end())
            prev.y = crd.y;
    }
    return prev;
}

int main(int argc, char** argv)
{
    std::chrono::time_point<std::chrono::high_resolution_clock> time_start;
    std::chrono::time_point<std::chrono::high_resolution_clock> time_end;

    level_t level = load_level(level1);

    if(SDL_Init(SDL_INIT_VIDEO) != 0) {
        fprintf(stderr, "\nUnable to initialize SDL:  %s\n", SDL_GetError());
        return EXIT_FAILURE;
    }
    auto sdl_quit_guard = make_scope_guard(SDL_Quit);

    auto window = to_unique(
        SDL_CreateWindow("Hello World",
                         SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                         640, 480,
                         SDL_WINDOW_SHOWN),
        SDL_DestroyWindow);

    auto renderer = to_unique(
        SDL_CreateRenderer(window.get(), -1, SDL_RENDERER_ACCELERATED
                                             | SDL_RENDERER_PRESENTVSYNC),
        SDL_DestroyRenderer);
    SDL_SetRenderDrawColor(renderer.get(), 200, 180, 130, 255);

    auto tiles_surface = to_unique(
        SDL_LoadBMP("tiles.bmp"),
        SDL_FreeSurface);
    SDL_SetColorKey(tiles_surface.get(), SDL_TRUE, 0xFFFF00FF);

    auto tiles_texture = to_unique(
        SDL_CreateTextureFromSurface(renderer.get(), tiles_surface.get()),
        SDL_DestroyTexture);

    tiles_surface.reset();

    while(true)
    {
        time_start = std::chrono::high_resolution_clock::now();

        SDL_Event ev;
        if(SDL_PollEvent(&ev)) switch(ev.type)
        {
        case SDL_QUIT:
            return EXIT_SUCCESS;
        case SDL_KEYDOWN:
            switch(ev.key.keysym.sym)
            {
            case SDLK_ESCAPE: return EXIT_SUCCESS;
            case SDLK_UP: level.ralph.movement += { 0, -1 }; break;
            case SDLK_DOWN: level.ralph.movement += { 0, 1 }; break;
            case SDLK_LEFT: level.ralph.movement += { -1, 0 }; break;
            case SDLK_RIGHT: level.ralph.movement += { 1, 0 }; break;
            }
            break;
        case SDL_KEYUP:
            switch(ev.key.keysym.sym)
            {
            case SDLK_UP: level.ralph.movement -= { 0, -1 }; break;
            case SDLK_DOWN: level.ralph.movement -= { 0, 1 }; break;
            case SDLK_LEFT: level.ralph.movement -= { -1, 0 }; break;
            case SDLK_RIGHT: level.ralph.movement -= { 1, 0 }; break;
            }
            break;
        }

        level.ralph.movement = {0,0};
        const Uint8 *keys = SDL_GetKeyboardState(NULL);
        if(keys)
        {
        if(keys[SDL_SCANCODE_UP])
            level.ralph.movement += { 0, -1 };
        if(keys[SDL_SCANCODE_DOWN])
            level.ralph.movement += { 0, 1 };
        if(keys[SDL_SCANCODE_LEFT])
            level.ralph.movement += { -1, 0 };
        if(keys[SDL_SCANCODE_RIGHT])
            level.ralph.movement += { 1, 0 };
        }

        auto tc = terrain_collisions(level, ralph_bbox(level.ralph.position));
        coord conveyor = { 0, 0 };
        for(auto t : tc) switch(t)
        {
        case TER_UP: conveyor += { 0, -1 }; break;
        case TER_DOWN: conveyor += { 0, 1 }; break;
        case TER_LEFT: conveyor += { -1, 0 }; break;
        case TER_RIGHT: conveyor += { 1, 0 }; break;
        case TER_SPIKES:
            level.ralph.position = level.ralph.start_position;
            break;
        default: break;
        }

        coord new_pos = level.ralph.position
                        + vec_mul(level.ralph.movement, 4)
                        + vec_mul(conveyor, 2);

        level.ralph.position = try_move(level, level.ralph.position, 
                                        vec_mul(level.ralph.movement, 4)
                                        + vec_mul(conveyor, 3));

        SDL_RenderClear(renderer.get());

        auto draw_sprite = [&](coord crd, coord texi)
        {
            if(texi == coord{0, 0})
                return;
            SDL_Rect from = { texi.x * tile_size, texi.y * tile_size,
                              tile_size, tile_size };
            SDL_Rect to = { crd.x, crd.y, tile_size, tile_size };
            SDL_RenderCopy(renderer.get(), tiles_texture.get(),
                           &from, &to);
        };

        for(coord i : dimen_range(level.map.dimensions()))
        {
            coord texi = texture_index(level.map[i]);
            draw_sprite({i.x * tile_size, i.y * tile_size }, texi);
        }

        for(auto const& enemy : level.enemies)
        {
            draw_sprite(enemy.position, { 0, 1 });
        }

        draw_sprite(level.ralph.position, { 1, 0 });


        SDL_RenderPresent(renderer.get());

        using namespace std::literals::chrono_literals;
        time_end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double, std::milli> time_ms
            = 33ms - (time_end - time_start);
        if(time_ms.count() > 0)
            SDL_Delay(time_ms.count());
    }

    return 0;
}
