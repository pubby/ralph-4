#ifndef INT2D_COORD_SYS_VIEW_HPP
#define INT2D_COORD_SYS_VIEW_HPP

#include <cassert>

#include "matrix.hpp"
#include "units.hpp"

namespace int2d {

    // Allows transforming a rect or coordinate system into a new coordinate
    // system with an origin at (0,0).
    // REMEMBER: x=right, y=down. This affects rotations!
    class coord_sys_view
    {
    public:
        coord_sys_view()
        : coord_sys_view(coord{0,0})
        {}

        explicit coord_sys_view(coord origin)
        : m_mat(imat3x3::translate(origin))
        , m_inverse(imat3x3::translate(-origin))
        , m_dim{0,0}
        {}

        explicit coord_sys_view(rect subrect)
        : m_mat(imat3x3::translate(subrect.c))
        , m_inverse(imat3x3::translate(-subrect.c))
        , m_dim(subrect.d)
        {}

        coord_sys_view(coord_sys_view const& cs, coord origin)
        : m_mat(imat3x3::translate(origin) * cs.m_mat)
        , m_inverse(cs.m_inverse * imat3x3::translate(-origin))
        , m_dim{0,0}
        {
            assert((cs.dimensions() == dimen{0,0}));
        }

        coord_sys_view(coord_sys_view const& cs, rect subrect)
        : m_mat(imat3x3::translate(subrect.c) * cs.m_mat)
        , m_inverse(cs.m_inverse * imat3x3::translate(-subrect.c))
        , m_dim(subrect.d)
        {
            assert(in_bounds(subrect, cs.dimensions()));
        }

        // Returns {0,0} for coordinate systems.
        dimen dimensions() const { return m_dim; }

        void rotate_cw(int quarter_turns)
        {
            apply_matrix(imat3x3::rotate_cw(quarter_turns),
                         imat3x3::rotate_cw(-quarter_turns));
        }

        void rotate_ccw(int quarter_turns)
        {
            apply_matrix(imat3x3::rotate_ccw(quarter_turns),
                         imat3x3::rotate_ccw(-quarter_turns));
        }

        void hmirror()
        {
            apply_matrix(imat3x3::hmirror(), imat3x3::hmirror());
        }

        void vmirror()
        {
            apply_matrix(imat3x3::vmirror(), imat3x3::vmirror());
        }

        // Converts back to the system passed into the constructor.
        template<typename T>
        auto to_parent(T&& t) const { return transform(m_mat, t); }

        // Inverse of 'to_parent'.
        template<typename T>
        auto from_parent(T&& t) const { return transform(m_inverse, t); }

        imat3x3 matrix() const { return m_mat; }
        imat3x3 inverse_matrix() const { return m_inverse; }
    private:
        void apply_matrix(imat3x3 mat, imat3x3 inv)
        {
            if(area(dimensions()) != 0)
            {
                rect r = transform(mat, rect{ {0,0}, m_dim });
                m_mat = mat * imat3x3::translate(-r.c) * m_mat;
                m_inverse *= imat3x3::translate(r.c) * inv;
                m_dim = r.d;
            }
            else
            {
                m_mat = mat * m_mat;
                m_inverse *= inv;
            }
        }

        imat3x3 m_mat;
        imat3x3 m_inverse;
        dimen m_dim;
    };

}

#endif
