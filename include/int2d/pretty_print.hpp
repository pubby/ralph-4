#ifndef INT2D_PRETTY_PRINT_HPP
#define INT2D_PRETTY_PRINT_HPP

#include <ostream>

#include "line.hpp"
#include "units.hpp"

namespace int2d {
    inline std::ostream& operator<<(std::ostream& stream, dimen dim)
    {
        stream << "dimen{ " << dim.w << ", " << dim.h << " }";
        return stream;
    }

    inline std::ostream& operator<<(std::ostream& stream, coord crd)
    {
        stream << "coord{ " << crd.x << ", " << crd.y << " }";
        return stream;
    }

    inline std::ostream& operator<<(std::ostream& stream, rect r)
    {
        stream << "rect{ " << r.c << ", " << r.d << " }";
        return stream;
    }

    inline std::ostream& operator<<(std::ostream& stream, line_state st)
    {
        stream << "line_state{ " << st.position << ", "
                                 << st.direction << ", "
                                 << st.error << " }";
        return stream;
    }
}

#endif
